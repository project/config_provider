<?php

namespace Drupal\Tests\config_provider\Kernel;

use Drupal\KernelTests\KernelTestBase;

/**
 * Test description.
 *
 * @group config_provider
 */
class ConfigCollectorTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'config_provider',
    'config_provider_foo_provider_test',
    'config_provider_foo_consumer_test',
  ];

  /**
   * Test Configuration Provider collector collects Foo provider and config.
   */
  public function testFooProvider() {
    // Avoid ConfigCollector deprecation message, requiring the install profile.
    $this->setInstallProfile('testing');

    $collector = $this->container->get('config_provider.collector');
    $collector->addInstallableConfig();
    $storage = $this->container->get('config_provider.storage');

    $this->assertTrue($storage->exists('foo.whatever.settings'));
    $foo = $storage->read('foo.whatever.settings');
    $this->assertTrue($foo['foo']);
    $this->assertFalse($foo['bar']);
  }

}
